<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizz_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quizz_lang_id')->unsigned()->index();
            $table->string('slug');
            $table->string('translation');
            $table->boolean('gender')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quizz_translations');
    }
}
