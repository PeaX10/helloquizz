<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeGenderType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quizz_langs', function(Blueprint $table){
            $table->integer('gender')->unsigned()->change();
        });
        Schema::table('quizz_translations', function(Blueprint $table){
            $table->integer('gender')->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quizz_langs', function(Blueprint $table){
            $table->boolean('gender')->change();
        });
        Schema::table('quizz_translations', function(Blueprint $table){
            $table->boolean('gender')->change();
        });
    }
}
