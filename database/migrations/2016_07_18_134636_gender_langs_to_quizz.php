<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GenderLangsToQuizz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quizz_langs', function(Blueprint $table){
            $table->dropColumn('gender');
        });
        Schema::table('quizzs', function(Blueprint $table){
            $table->string('gender')->after('image')->default('all');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quizzs', function(Blueprint $table){
            $table->dropColumn('gender');
        });
        Schema::table('quizz_langs', function(Blueprint $table){
            $table->tinyInteger('gender')->unsigned()->after('lang');
        });
    }
}
