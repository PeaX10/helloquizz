<div class="sidebar" data-color="red">
    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="logo">
        <a href="{{ url('/') }}" class="logo-text">
            HelloQuizz
        </a>
    </div>

    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ url('img/avatar/'.$user->avatar) }}" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    {{ $user->name }}
                    <b class="caret"></b>
                </a>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li><a href="#">Modifier mon profil</a></li>
                        <li><a href="#">Paramètres</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav">
            <li @if(empty($active)) class="active" @endif>
                <a href="{{ url('/') }}">
                    <i class="pe-7s-graph"></i>
                    <p>Tableau de bord</p>
                </a>
            </li>
            <li @if($active == "quizz") class="active" @endif>
                <a href="{{ url('quizz') }}">
                    <i class="fa fa-question"></i>
                    <p>Quizzs</p>
                </a>
            </li>

            <li @if($active == "result") class="active" @endif>
                <a href="{{ url('results') }}">
                    <i class="fa fa-picture-o"></i>
                    <p>Résultats</p>
                </a>
            </li>

            <li @if($active == "translation") class="active" @endif>
                <a href="{{ url('translations') }}">
                    <i class="fa fa-globe"></i>
                    <p>Traductions</p>
                </a>
            </li>

            <li @if($active == "user") class="active" @endif>
                <a href="{{ url('users') }}">
                    <i class="fa fa-users"></i>
                    <p>Utilisateurs</p>
                </a>
            </li>

            <li @if($active == "stat") class="active" @endif>
                <a href="{{ url('stats') }}">
                    <i class="fa fa-line-chart"></i>
                    <p>Statistiques</p>
                </a>
            </li>

            <li @if($active == "setting") class="active" @endif>
                <a href="{{ url('settings') }}">
                    <i class="fa fa-gears"></i>
                    <p>Paramètres</p>
                </a>
            </li>
        </ul>
    </div>
</div>