@extends('back.layouts.default', ['active' => 'quizz'])

@section('title') Modification Quizz @endsection

@section('content')
    <div class="row">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="content">
                        <h4>
                            Réponses possibles <small><span class="label label-info">{{ count($answers) }}</span></small>
                            <a class="pull-right btn btn-round btn-danger btn-fill" href="{{ url('quizz/'.$quizz->id.'/answer/create') }}"><i class="fa fa-plus"></i></a>
                        </h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Fond</th>
                                    <th>En Ligne?</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($answers as $answer)
                                    <tr>
                                        <td>{{ $answer->id }}</td>
                                        <td><img src="{{ url('img/answer/'.$answer->background) }}" class="img-responsive img-raised img-rounded" style="max-width: 400px"></td>
                                        <td>@if($answer->visible) <span class="label label-success">Oui</span> @else <span class="label label-danger">Non</span> @endif</td>
                                        <td>
                                            <a rel="tooltip" title="" class="btn btn-simple btn-info btn-icon table-action builder" href="{{ url('quizz/'.$quizz->id.'/answer/'.$answer->id) }}" data-original-title="Construire"><i class="fa fa-puzzle-piece"></i></a>
                                            <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action edit" href="{{ url('quizz/'.$quizz->id.'/answer/'.$answer->id.'/edit') }}" data-original-title="Modifier"><i class="fa fa-edit"></i></a>
                                            <a rel="tooltip" title="" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)" data-original-title="Supprimer"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="content">
                        <h4>
                            Langues du quizz <small><span class="label label-info">{{ count($langs) }}</span></small>
                            <a class="pull-right btn btn-round btn-danger btn-fill" href="{{ url('quizz/'.$quizz->id.'/lang/add') }}"><i class="fa fa-plus"></i></a>
                        </h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Langue</th>
                                    <th>En Ligne?</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($langs as $lang)
                                    <tr>
                                        <td>{{ $lang->id }}</td>
                                        <td><img src="{{ url('img/flags/'.$lang->lang.'.png') }}"></td>
                                        <td>@if($lang->visible) <span class="label label-success">Oui</span> @else <span class="label label-danger">Non</span> @endif</td>
                                        <td>
                                            @if($lang->visible)
                                                <a rel="tooltip" title="" class="btn btn-simple btn-success btn-icon table-action offline" href="{{ url('quizz/'.$quizz->id.'/lang/'.$lang->id.'/edit') }}" data-original-title="Mettre hors-ligne"><i class="fa fa-toggle-on"></i></a>
                                            @else
                                                <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action online" href="{{ url('quizz/'.$quizz->id.'/lang/'.$lang->id.'/edit') }}" data-original-title="Mettre en ligne"><i class="fa fa-toggle-off"></i></a>
                                            @endif
                                            <a rel="tooltip" title="" class="btn btn-simple btn-danger btn-icon table-action remove" href="javascript:void(0)" data-original-title="Supprimer"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="content">
                        <h3>
                            @if(!empty($title = $quizz->quizzLangs()->where('lang', strtolower(App::getLocale()))->where('visible', true)->first()))
                                {{ $title->translations->title }}
                            @else
                                {{ $quizz->slug }}
                            @endif
                            <small><span class="label label-info">ID: {{ $quizz->id }}</span></small>
                        </h3>
                        <hr>
                        <div clas="col-md-12 col-md-offset-1">
                            {!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->action( route('quizz.update', $quizz) )->put()->enctype("multipart/form-data") !!}
                            {!! BootForm::bind($quizz) !!}
                            {!! BootForm::text('Slug', 'slug') !!}
                            {!! BootForm::select('Sexe', 'gender')->options(['all' => 'Homme et Femme', 'male' => 'Homme', 'female' => 'Femme']) !!}
                            {!! BootForm::select('En Ligne?', 'visible')->options([false => 'Non', 1 => 'Oui']) !!}
                            <div class="form-group">
                                <label class="col-sm-4 col-lg-2 control-label" for="visible">Aperçu</label>
                                <div class="col-sm-8 col-lg-10">
                                    <img src="{{ url('img/quizz/'.$quizz->image) }}" class="img-responsive img-rounded img-raised">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 col-lg-2 control-label" for="visible">Photo</label>
                                <div class="col-sm-8 col-lg-10">
                                    <label><small>Minimum 800x400 (2MB MAX)</small></label>
                                    <input 	type='file'
                                              class='input-ghost'
                                              name='image'
                                              style='visibility:hidden; height:0'
                                              onchange="$(this).next().find('input').val(($(this).val()).split('\\').pop());">
                                    <div class="input-group input-file" name="Fichier_1">
                                        <span class="input-group-btn">
                                            <button 	class="btn btn-info btn-info btn-fill btn-choose"
                                                       type="button"
                                                       onclick="$(this).parents('.input-file').prev().click();">Choisir</button>
                                        </span>
                                        <input 	type="text"
                                                  class="form-control"
                                                  placeholder='Choisissez un fichier...'
                                                  style="cursor:pointer"
                                                  onclick="$(this).parents('.input-file').prev().click(); return false;"
                                        />
                                    </div>
                                </div>
                            </div>
                            {!! BootForm::submit('Modifier')->class('btn btn-rounded btn-fill pull-right') !!}
                            {!! BootForm::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="content">
                        <h4>Les 30 dernières visites <small><span class="label label-info">total: {{ count($visits) }}</span></small></h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Utilisateur</th>
                                    <th>Langue</th>
                                    <th>IP</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($visits->take(30) as $visit)
                                <tr>
                                    <td>{{ $visit->id }}</td>
                                    <td><a href="{{ url('user/'.$visit->user()->first()->id.'/edit') }}">{{ $visit->user()->first()->name }}</a></td>
                                    <td><img src="{{ url('img/flags/'.$visit->quizzLang()->first()->lang.'.png') }}"></td>
                                    <td>{{ $visit->ip }}</td>
                                    <td>{{ \Carbon\Carbon::parse($visit->created_at)->diffForHumans() }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="content">
                        <h4>Les 30 derniers résultats générés <small><span class="label label-info">{{ count($results) }}</span></small></h4>
                        <hr>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Utilisateur</th>
                                    <th>Image</th>
                                    <th>Langue</th>
                                    <th>IP</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results->take(30) as $result)
                                    <tr>
                                        <td>{{ $result->id }}</td>
                                        <td><a href="{{ url('user/'.$result->user()->first()->id.'/edit') }}">{{ $result->user()->first()->name }}</a></td>
                                        <td><img style="max-width: 150px" src="{{ url('img/result/'.$result->image) }}" class="img-responsive img-raised img-rounded"></td>
                                        <td><img src="{{ url('img/flags/'.$result->quizzLang()->first()->lang.'.png') }}"></td>
                                        <td>{{ $result->ip }}</td>
                                        <td>{{ \Carbon\Carbon::parse($result->created_at)->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        @if(Session::has('edited_quizz'))
            $.notify({
            icon: 'pe-7s-bell',
            message: "<b>Félicitation</b> - votre quizz a bien été modifié."

        },{
            type: 'success',
            timer: 4000
        });
        @endif
    </script>
@endsection