@extends('front.layouts.boxed')

@section('title')@lang('app.home_title')@endsection
@section('css')<link href="{{ url('css/home.css') }}" rel="stylesheet" type="text/css">@endsection
@section('header')
    @include('front.home.featured_quizzs')
@endsection

@section('content')
    <h3>@lang('app.latest_tests')</h3>
    <hr>
    <div class="row quizzs">
        @include('front.home.quizzs')
    </div>

    @if($quizzs->hasMorePages())
    <div class="row" align="center">
        <div class="col-md-4 col-md-offset-4">
            <a href="#2" class="btn btn-rose" id="showMore">@lang('app.show_more')</a>
        </div>
    </div>
    @endif
@endsection

@section('js')
    <script type="text/javascript">
        var pageMax = {{ $quizzs->lastPage() }}

        $(document).ready(function() {
            $(document).on('click', '#showMore', function (e) {
                var nextPage = parseInt($(this).attr('href').split('#')[1]);
                getQuizzs(nextPage);
                nextPage = nextPage + 1
                if(nextPage > pageMax){
                    $(this).parent().parent().remove();
                }else{
                    $(this).attr('href', '#'+ nextPage);
                }
            });
        });

        function getQuizzs(page) {
            $.ajax({
                url : '?page=' + page,
            }).done(function (data) {
                $('.quizzs').append(data);
                location.hash = page;
            }).fail(function () {
                alert("@lang('app.test_not_loaded')");
            });
        }
    </script>
@endsection