@foreach($featuredQuizzs as $key => $quizz)
    <div class="item @if($key == 1){{ 'active' }}@endif">
        <div class="page-header header-filter" style="background-image: url('{{ url('img/quizz/'.$quizz->image) }}');">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <h2 class="title">{{ $quizz->translations->title }}</h2>
                        <h4>{{ $quizz->translations->description }}</h4>
                        <a href="{{ url('/q/'.$quizz->slug) }}" class="btn btn-info btn-rose">@lang('app.do_test')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach