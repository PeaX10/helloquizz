@foreach($quizzs as $quizz)
    <div class="col-md-4 col-sm-6">
        <div class="card card-background" style="background-image: url('{{ url('img/quizz/'.$quizz->image) }}')">
            <div class="content">
                <a href="{{ url('/q/'.$quizz->slug) }}">
                    <h3 class="card-title">{{ $quizz->translations->title }}</h3>
                </a>
                <p class="card-description">
                    {{ $quizz->translations->description }}
                </p>
                <a href="{{ url('/q/'.$quizz->slug) }}" class="btn btn-danger btn-round">
                    <i class="material-icons">arrow_forward</i> @lang('app.do_test')
                    <div class="ripple-container"></div>
                </a>
            </div>
        </div>
    </div>
@endforeach