@extends('front.layouts.default')

@section('title')@lang('app.home_title')@endsection

@section('content')
    <div class="row" align="center">
        <div class="col-xs-12 col-md-4 col-md-offset-4">
            <img src="{{ url('img/avatar/'.$user->avatar) }}" alt="" class="img-circle img-responsive" style="max-width: 250px;" alt="Helloquizz - {{ $user->name }}">
            <h3>{{ $user->name }}</h3>
            <a href="{{ url('/logout') }}" class="btn btn-danger">@lang('app.logout')</a>
        </div>
    </div>
@endsection