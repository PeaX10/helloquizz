<nav class="navbar navbar-inverse @if(!empty($absolute) && $absolute) navbar-absolute @endif" role="navigation">
    <div class="container">
        <div class="navbar-header navbar-brand">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ url('/') }}"><img src="{{ url('img/logo.png') }}" alt="Logo HelloQuizz" /></a>
            <div class="fb-like" data-href="https://www.facebook.com/HelloQuizzFr/" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="langs">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle " data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ url('img/flags/'.App::getLocale().'.png') }}">
                        {{ App::getLocale() }}
                        <b class="caret"></b>
                        <div class="ripple-container"></div></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://en.{{ config('app.domain') }}"><img src="{{ url('img/flags/en.png') }}"> English</a></li>
                        <li><a href="http://fr.{{ config('app.domain') }}"><img src="{{ url('img/flags/fr.png') }}"> Français</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><img src="{{ url('img/flags/DE.png') }}"> Deutsch <span class="label label-default">@lang('app.menu_soon')</span></a></li>
                        <li><a href="#"><img src="{{ url('img/flags/RO.png') }}"> Română <span class="label label-default">@lang('app.menu_soon')</span></a></li>
                        <li><a href="#"><img src="{{ url('img/flags/IT.png') }}"> Italiano <span class="label label-default">@lang('app.menu_soon')</span></a></li>
                        <li><a href="#"><img src="{{ url('img/flags/ES.png') }}"> Español <span class="label label-default">@lang('app.menu_soon')</span></a></li>
                        <li><a href="#"><img src="{{ url('img/flags/BR.png') }}"> Português <span class="label label-default">@lang('app.menu_soon')</span></a></li>
                        <li><a href="#"><img src="{{ url('img/flags/JP.png') }}"> 日本語 <span class="label label-default">@lang('app.menu_soon')</span></a></li>
                    </ul>
                </li>
                @if(Auth::check())
                    <?php $user = Auth::user(); ?>
                    <li>
                        <a href="{{ url('me') }}" class="profile-photo">
                            <div class="profile-photo-small">
                                <img src="{{ url('img/avatar/'.$user->avatar) }}" alt="" class="img-circle img-responsive">
                            </div>
                        </a>
                    </li>
                @else
                    <li>
                        <a href="{{ url('auth/facebook') }}" class="profile-photo">
                            <div class="profile-photo-small">
                                <img src="img/faces/no-avatar.gif" alt="" class="img-circle img-responsive">
                            </div>
                        </a>
                    </li>
                @endif

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-->
</nav>