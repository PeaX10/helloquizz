<!DOCTYPE html>
<html>
    <head>
        <title>HelloQuizz - @yield('title')</title>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="description" content="@lang('app.meta_description')">
        <meta name="keywords" content="@lang('app.meta_keywords')">
        <meta name="author" content="PeaX">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

        <link rel="apple-touch-icon" sizes="76x76" href="{{ url('img/apple-icon.png') }}">
        <link rel="icon" type="image/png" href="{{ url('img/favicon.png') }}">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <link href="{{ url('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('css/material-kit.css') }}" rel="stylesheet" type="text/css">
        @yield('css')
    </head>
    <body>
    @include('front.layouts.menu', ['absolute' => true])
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <div class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner">
                @yield('header')
            </div>

            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <i class="material-icons">keyboard_arrow_left</i>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <i class="material-icons">keyboard_arrow_right</i>
            </a>
        </div>
    </div>
    <div class="main main-raised">
        <div class="container">
            @yield('content')
        </div>
    </div>
    @include('front.layouts.footer')
    <script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/jquery.dropdown.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/material.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/material-kit.js') }}"></script>
    @yield('js')
    </body>
</html>