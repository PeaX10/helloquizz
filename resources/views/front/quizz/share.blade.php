<div class="row share">
    @if($hr_pos == 'top')<div class="col-md-12"><hr></div>@endif
    <div class="col-md-10" align="center">
        <a class="btn btn-facebook btn-fill btn-lg btn-block" href="#" onclick="open('https://www.facebook.com/sharer/sharer.php?m2w&s=100&p[title]={{ urlencode($quizz->translations->title) }}&p[summary]={{ urlencode($quizz->translations->result_description) }}&p[url]={{ urlencode(url('q/'.$quizz->slug)) }}}&p[images][0]={{ urlencode(url('img/result/'.$result->image)) }}', '@lang("test_share_fb")', 'toolbar=no, status=no, menubar=no,height=320,width=680,top=150,left='+((screen.width/2) - 340)); return false;"><i class="fa fa-facebook-square"></i> @lang('app.test_share_fb')</a>
    </div>
    <div class="col-md-2" align="center">
        <a class="btn btn-twitter btn-fill btn-lg btn-block" href="#" onclick="open('{{ Share::load(url('r/'.$result->slug), $quizz->translations->title, url('img/result/'.$result->image))->twitter() }}', '@lang("test_share_fb")', 'toolbar=no, status=no, menubar=no,height=320,width=680,top=150,left='+((screen.width/2) - 340)); return false;"><i class="fa fa-twitter-square"></i><span class="hidden-md hidden-lg"> @lang('app.test_share_twitter')</span></a>
    </div>
        @if($hr_pos == 'bottom')<div class="col-md-12"><hr></div>@endif
</div>