@extends('front.layouts.2columns')

@section('title'){{ $quizz->translations->title }}@endsection

@section('header')
    <div class="page-header header-filter" data-parallax="active" filter-color="black" style="background-image: url('{{ url('img/quizz/'.$quizz->image) }}');">
        <div class="container">
            <h1 class="title text-center">{{ $quizz->translations->title }}</h1>
        </div>
    </div>
@endsection

@section('left_column')
    @include('front.quizz.share', ['hr_pos' => 'bottom'])
    <h2>
        {{ $quizz->translations->title }}
        <br>
    </h2>
    <img src="{{ url('img/result/'.$result->image) }}" alt="Résultat de {{ $result->user->name }}, {{ $quizz->translations->title }}- HelloQuizz" class="img-rounded img-responsive img-raised"/>
    <br>
    <h5>{{ $quizz->translations->description }}</h5>
    @if(Auth::user()->id == $result->user_id)
        <div class="col-md-12">
            <a class="btn btn-round btn-rose" id="doQuizz" href="{{ url('d/'.$quizz->slug) }}"><i class="material-icons">repeat</i> @lang('app.test_retry')</a>
        </div>
    @else
        <div class="col-md-12">
            <a class="btn btn-round btn-rose" id="doQuizz" href="{{ url('d/'.$quizz->slug) }}"><i class="material-icons">forward</i> @lang('app.do_test_now')</a>
        </div>
    @endif
    @include('front.quizz.share', ['hr_pos' => 'top'])
@endsection

@section('right_column')
    <div class="hidden-sm hidden-xs">
        @include('front.quizz.featured_quizzs')
    </div>
@endsection

@section('bottom')
    <div class="quizzs">
        @include('front.quizz.quizzs')
    </div>

    @if($quizzs->hasMorePages())
        <div class="row" align="center">
            <div class="col-md-4 col-md-offset-4">
                <a href="#2" class="btn btn-rose" id="showMore">@lang('app.show_more')</a>
            </div>
        </div>
    @endif

    <div class="load-result" style="display: none; background: rgba(255, 255, 255, 0.92); height: 100%; width: 100%; position: fixed; z-index: 1030; top:0; left: 0">
        <div class="vertical-center">
            <div class="text-center col-md-4 col-md-offset-4">
                <h2 class="title">@lang('app.test_load')<span id="loading">.</span></h2>
                <br>
                <img src="{{ url('img/loader.gif') }}" alt="Loader HelloQuizz" />
            </div>
        </div>

    </div>
@endsection

@section('js')
    <script type="text/javascript">
        var pageMax = {{ $quizzs->lastPage() }}

        $(document).ready(function() {
            $(document).on('click', '#showMore', function (e) {
                var nextPage = parseInt($(this).attr('href').split('#')[1]);
                getQuizzs(nextPage);
                nextPage = nextPage + 1
                if(nextPage > pageMax){
                    $(this).parent().parent().remove();
                }else{
                    $(this).attr('href', '#'+ nextPage);
                }
            });
        });

        function getQuizzs(page) {
            $.ajax({
                url : '?page=' + page,
            }).done(function (data) {
                $('.quizzs').append(data);
                location.hash = page;
            }).fail(function () {
                alert("@lang('app.test_not_loaded')");
            });
        }
        $('.load-result').hide();
        var nb = 1;
        $('#doQuizz').on('click', function(){
            $('.load-result').show();
            setInterval(function(){
                nb += 1;
                if(nb <= 3){
                    $("span#loading").append(".");
                }else{
                    nb = 1;
                    $("span#loading").html(".");
                }
            }, 250);
        });
    </script>
@endsection