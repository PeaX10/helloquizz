<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTML Language Lines
    |--------------------------------------------------------------------------
    | Description
    |
    */

    'meta'  =>  [   'description'   => 'HelloQuizz: Browse numerous personality tests to find out more about yourself and your loved ones, share the results with your friends!',
                    'keywords'      => 'quizz, quizzs, test, tests, fun, love, facebook'
                ],
    'home'  =>  [   'title'         => 'Best Test on the web',
                    'latest_quizzes' => 'Latest tests',
                    'no_quizze'     => 'There are no tests',
                    'do_quizze'     => 'Let\'s do it!',
                    'show_more'     => 'See more',
                    'quizz_not_loaded' => 'Tests could not be loaded.',

                ],
    'quizz' =>  [   'do_quizz_now'  =>  'Take the test now',
                    'load'          =>  'Analyzing profile'

    ],
    'menu'  =>  [   'soon'          =>  'soon',

                ],
    'me'    =>  [   'logout'        =>  'Logout'],

];
