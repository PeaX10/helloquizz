<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class BackOffice
{

    protected $adminUsers = ['1'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::check() || !$this->isAdmin(Auth::user()->id)){
            abort(404);
        }

        return $next($request);
    }

    public function isAdmin($user_id){
        return in_array($user_id, $this->adminUsers);
    }
}
