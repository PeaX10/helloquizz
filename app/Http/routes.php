<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
# Back Office
Route::group(['domain' => 'bo'.env('SESSION_DOMAIN'), 'middleware' => 'bo'], function () {
    Route::get('/', 'Admin\PageController@index');
    # Quizz
    Route::resource('quizz', 'Admin\QuizzController', ['except' => ['show']]);
    Route::group(['prefix' => '/quizz/{id}'], function(){
        Route::resource('answer', 'Admin\AnswerController');
        Route::resource('lang', 'Admin\QuizzLangController', ['except' => ['show']]);
    });
});

Route::get('/', 'PageController@home')->name('home');
Route::get('/me', 'PageController@me')->middleware('auth');

Route::get('/q/{slug}', 'QuizzController@show');
Route::get('/r/{slug}', 'QuizzController@result');


# Social Auth
Route::get('auth/facebook', 'Auth\AuthSocialController@login');
Route::get('auth/facebook/callback', 'Auth\AuthSocialController@facebookCallback');
Route::get('/logout', 'Auth\AuthSocialController@logout');