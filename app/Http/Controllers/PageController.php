<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Quizz\Quizz;
use Illuminate\Support\Facades\App;
use App\Quizz\QuizzLang;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function home(Request $request){
        // Tout les quizz en ligne dans la langue du client
        // Attention le select est à faire dans cette ordre sinon le ID de lang_quizz va être remplacer par celui de quizzs
        $quizzs = QuizzLang::visible()->with('quizzTranslations')->select('quizzs.*', 'quizz_langs.*');

        // On veut les 3 meilleurs quizzs parmis ceux là en fonction de leur nombre de fois joué durant ces 2 semaines

        $featuredQuizzs = $quizzs->get()->load('quizzResults2WeeksCount', 'quizzVisits2WeeksCount')->sortByDesc(function($quizz){
            $plays = $quizz->quizzResults2WeeksCount;
            $visits = $quizz->quizzVisits2WeeksCount;
            return $plays * 5 + $visits;
        })->take(3);

        $quizzs = $quizzs->orderby('quizzs.id', 'desc')->paginate(6);
        // Afficher plus de quizz en ajax
        if($request->ajax()){
            return view('front.home.quizzs', compact('quizzs'));
        }
        
        return view('front.home.index', compact('quizzs', 'featuredQuizzs'));
    }

    public function me(){
        return view('front.pages.me');
    }
}
