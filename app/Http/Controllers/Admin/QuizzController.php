<?php

namespace App\Http\Controllers\Admin;

use App\Quizz\QuizzLang;
use Illuminate\Http\Request;
use App\Quizz\Quizz;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;

class QuizzController extends Controller
{
    public function index(){
        $quizzs = Quizz::orderBy('id', 'desc')->get()->load('quizzLangs');
        return view('back.quizz.index', compact('quizzs'));
    }

    public function destroy(Request $request, $id){
        if($request->ajax()){
            $quizz = Quizz::findOrFail($id);
            $quizz->delete();
            return ['success' => true];
        }else{
            return abort(404);
        }
    }

    public function create(){
        return view('back.quizz.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'slug' => 'required|unique:quizzs,slug|min:5',
            'gender' => 'required|in:all,male,female',
            'image' => 'required|image|max:2048|dimensions:min_width=800,min_height=400'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $slug = str_slug(Input::get('slug'));
            if(Quizz::where('slug', '=', $slug)->exists()) {
                $validator->errors()->add('slug', 'Ce slug existe déjà !');
                return back()->withErrors($validator)->withInput();
            }else{
                $image_name =  'q_'.time().str_random(rand(15, 20)).'.jpg';
                $image = Image::make(Input::file('image'))->save(public_path('img/quizz/'.$image_name));
                $quizz = new Quizz();
                $quizz->slug = $slug;
                if(Input::get('visible')) $quizz->visible = 1;
                $quizz->image = $image_name;
                $quizz->gender = Input::get('gender');
                $quizz->save();

                foreach(Input::get('langs') as $key => $value){
                    $lang = new QuizzLang();
                    $lang->quizz_id = $quizz->id;
                    $lang->lang = $value;
                    $lang->save();
                }

                return redirect('quizz')->with('added_quizz', true);
            }
        }
    }

    public function edit($id){
        $quizz = Quizz::findOrFail($id);
        $langs = $quizz->quizzLangs()->get();
        $results = $visits = collect();
        $answers = $quizz->answers()->get();
        foreach($langs as $key => $value){
            $results = $results->merge($value->quizzResults()->get());
            $visits = $visits->merge($value->quizzVisits()->get());
        }
        return view('back.quizz.edit', compact('quizz', 'langs', 'visits', 'results', 'answers'));
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'slug' => 'required|min:5',
            'gender' => 'required|in:all,male,female',
            'image' => 'image|max:2048|dimensions:min_width=800,min_height=400'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            // On encode notre slug et on regarde si il existe ou non en BDD
            $slug = str_slug(Input::get('slug'));
            if(Quizz::where('slug', '=', $slug)->whereNotIn('id', [$id])->exists()) {
                $validator->errors()->add('slug', 'Ce slug existe déjà !');
                return back()->withErrors($validator)->withInput();
            }else{
                $quizz = Quizz::findOrFail($id);
                if($request->hasFile('image')){
                    $image_name =  'q_'.time().str_random(rand(15, 20)).'.jpg';
                    $image = Image::make(Input::file('image'))->save(public_path('img/quizz/'.$image_name));
                    $quizz->image = $image_name;
                }
                $quizz->slug = $slug;
                if(Input::get('visible')) $quizz->visible = 1; else $quizz->visible = 0;
                $quizz->gender = Input::get('gender');
                $quizz->save();

                return back()->with('edited_quizz', true);
            }
        }
    }
}
