<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Quizz\QuizzAnswer;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class AnswerController extends Controller
{
    public function create($quizz_id){
        return view('back.quizz.answer.create', compact('quizz_id'));
    }

    public function store(Request $request, $quizz_id){
        $validator = Validator::make($request->all(), [
            'nb_text' => 'numeric|min:0|max:10',
            'nb_img' => 'numeric|min:0|max:10',
            'gender' => 'required|in:all,male,female',
            'image' => 'required|image|max:4096|dimensions:width=1200,height=630'
        ]);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $image_name =  'a_'.time().str_random(rand(15, 20)).'.jpg';
            $image = Image::make(Input::file('image'))->save(public_path('img/answer/'.$image_name));
            $answer = new QuizzAnswer();
            $answer->quizz_id = $quizz_id;
            if(Input::get('visible')) $answer->visible = 1;
            $answer->background = $image_name;
            $answer->gender = Input::get('gender');
            $builder = array();
            for ($i = 0; $i < Input::get('nb_text'); $i++) {
                $builder[$i] = ['type' => 'text', 'pos_x' => 100, 'pos_y' => 100, 'width' => 500, 'height' => 50, 'angle' => 0];
            }

            for ($i = Input::get('nb_text'); $i < Input::get('nb_text') + Input::get('nb_img'); $i++) {
                $builder[$i] = ['type' => 'img', 'pos_x' => 200, 'pos_y' => 300, 'width' => 300, 'height' => 300, 'angle' => 0];
            }
            $answer->builder = json_encode($builder);
            $answer->save();

            return redirect('quizz/'.$quizz_id.'/edit')->with('added_answer', true);

        }
    }
}
