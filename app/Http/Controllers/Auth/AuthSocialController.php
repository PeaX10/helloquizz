<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

class AuthSocialController extends Controller
{
    public function login(){
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback(){
        $fb_user = Socialite::driver('facebook')->user();
        $user = User::where('fb_id', $fb_user->id);

        if($user->count() != 0){
            Auth::login($user->first(), true);
            return back();
        }else{
            $user = new User();
            $user->fb_id = $fb_user->user['id'];
            $user->name = $fb_user->user['name'];
            $user->email = $fb_user->user['email'];
            $user->gender = $fb_user->user['gender'];
            $avatar_name = str_slug($user->name).'_'.str_random(40).'.jpg';
            Image::make($fb_user->avatar_original)->save(public_path('img/avatar/'.$avatar_name));
            $user->avatar = $avatar_name;
            $user->save();
            Auth::login($user, true);
            return back();
        }
    }


    public function logout(){
        Auth::logout();
        return back();
    }
}
