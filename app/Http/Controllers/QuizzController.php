<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Quizz\QuizzLang;
use App\Quizz\QuizzVisit;
use Illuminate\Support\Facades\Auth;
use App\Quizz\QuizzResult;

class QuizzController extends Controller
{
    public function show(Request $request, $slug){
        // Tout les quizz en ligne dans la langue du client
        // Attention le select est à faire dans cette ordre sinon le ID de lang_quizz va être remplacer par celui de quizzs
        $quizzs = QuizzLang::visible()->with('quizzTranslations')->select('quizzs.*', 'quizz_langs.*');

        // On veut les 3 meilleurs quizzs parmis ceux là en fonction de leur nombre de fois joué durant ces 2 semaines

        $featuredQuizzs = $quizzs->get()->load('quizzResults2WeeksCount', 'quizzVisits2WeeksCount')->sortByDesc(function($quizz){
            $plays = $quizz->quizzResults2WeeksCount;
            $visits = $quizz->quizzVisits2WeeksCount;
            return $plays * 5 + $visits;
        })->take(2);

        $quizzs = $quizzs->orderby('quizzs.id', 'desc')->paginate(6);
        // Afficher plus de quizz en ajax
        if($request->ajax()){
            return view('front.quizz.quizzs', compact('quizzs'));
        }

        $quizz = QuizzLang::visible()
                ->with('quizzTranslations')
                ->select('quizzs.*', 'quizz_langs.*')
                ->where('slug', $slug)
                ->firstOrFail();
        $this->updateVisit($request, $quizz->id);

        return view('front.quizz.show', compact('quizz', 'quizzs', 'featuredQuizzs'));
    }

    public function updateVisit(Request $request, $quizz_lang_id){
        // On vérifie si il n'y a pas eu de visite par le même utilisateur dans les 2 dernières minutes
        $nb_visits = QuizzVisit::where('ip', $request->ip())->where('quizz_lang_id', $quizz_lang_id)->where('created_at', '>=', Carbon::now()->subMinute(2))->count();
        if($nb_visits === 0){
            $visit = new QuizzVisit();
            $visit->user_id = Auth::user()->id;
            $visit->quizz_lang_id = $quizz_lang_id;
            $visit->ip = $request->ip();
            $visit->save();
        }
    }

    public function result(Request $request, $slug){
        // Tout les quizz en ligne dans la langue du client
        // Attention le select est à faire dans cette ordre sinon le ID de lang_quizz va être remplacer par celui de quizzs
        $quizzs = QuizzLang::visible()->with('quizzTranslations')->select('quizzs.*', 'quizz_langs.*');

        // On veut les 3 meilleurs quizzs parmis ceux là en fonction de leur nombre de fois joué durant ces 2 semaines

        $featuredQuizzs = $quizzs->get()->load('quizzResults2WeeksCount', 'quizzVisits2WeeksCount')->sortByDesc(function($quizz){
            $plays = $quizz->quizzResults2WeeksCount;
            $visits = $quizz->quizzVisits2WeeksCount;
            return $plays * 5 + $visits;
        })->take(2);

        $quizzs = $quizzs->orderby('quizzs.id', 'desc')->paginate(6);
        // Afficher plus de quizz en ajax
        if($request->ajax()){
            return view('front.quizz.quizzs', compact('quizzs'));
        }

        $result = QuizzResult::where('slug', $slug)->with('user')->firstOrFail();
        $quizz = QuizzLang::visible()
            ->with('quizzTranslations')
            ->select('quizzs.*', 'quizz_langs.*')
            ->where('quizz_langs.id', $result->quizz_lang_id)
            ->firstOrFail();

        return view('front.quizz.result', compact('quizz', 'result', 'quizzs', 'featuredQuizzs'));

    }
}
