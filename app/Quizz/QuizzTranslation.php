<?php

namespace App\Quizz;

use Illuminate\Database\Eloquent\Model;

class QuizzTranslation extends Model
{
    protected $table = "quizz_translations";

    protected $fillable = ['id', 'slug', 'quizz_lang', 'value', 'created_at',];

    public function quizzLang(){
        return $this->belongsTo('App\Quizz\QuizzLang');
    }

    public function scopeTrans($query, $slug){
        return $query->where('slug', $slug)->first()->translation;
    }
}
