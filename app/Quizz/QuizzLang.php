<?php

namespace App\Quizz;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Collection;

class QuizzLang extends Model
{
    protected $table = 'quizz_langs';

    protected $fillable = ['id', 'quizz', 'lang', 'visible', 'created_at',];

    public function quizzTranslations()
    {
        return $this->hasMany('App\Quizz\QuizzTranslation');
    }

    public function quizz()
    {
        return $this->belongsTo('App\Quizz\Quizz');
    }

    public function quizzVisits()
    {
        return $this->hasMany('App\Quizz\QuizzVisit');
    }

    public function quizzResults()
    {
        return $this->hasMany('App\Quizz\QuizzResult');
    }

    public function quizzVisitsCount()
    {
        return $this->quizzVisits()
            ->selectRaw('quizz_lang_id, count(*) as count')
            ->groupBy('quizz_lang_id');
    }

    public function quizzResultsCount()
    {
        return $this->quizzResults()
            ->selectRaw('quizz_lang_id, count(*) as count')
            ->groupBy('quizz_lang_id');
    }

    public function quizzVisits2WeeksCount()
    {
        return $this->quizzVisits()
            ->where('created_at', '>=', Carbon::now()->subWeek(2))
            ->selectRaw('quizz_lang_id, count(*) as count')
            ->groupBy('quizz_lang_id');
    }

    public function quizzResults2WeeksCount()
    {
        return $this->quizzResults()
            ->where('created_at', '>=', Carbon::now()->subWeek(2))
            ->selectRaw('quizz_lang_id, count(*) as count')
            ->groupBy('quizz_lang_id');
    }

    public function getQuizzVisitsCountAttribute()
    {
        if (!array_key_exists('quizzVisitsCount', $this->relations))
            $this->load('quizzVisitsCount');
        $related = $this->getRelation('quizzVisitsCount')->first();

        return ($related) ? (int)$related->count : 0;
    }

    public function getQuizzResultsCountAttribute()
    {
        if (!array_key_exists('quizzResultsCount', $this->relations))
            $this->load('quizzResultsCount');
        $related = $this->getRelation('quizzResultsCount')->first();

        return ($related) ? (int)$related->count : 0;
    }

    public function getQuizzVisits2WeeksCountAttribute()
    {
        if (!array_key_exists('quizzVisits2WeeksCount', $this->relations))
            $this->load('quizzVisitsCount');
        $related = $this->getRelation('quizzVisits2WeeksCount')->first();

        return ($related) ? (int)$related->count : 0;
    }

    public function getQuizzResults2WeeksCountAttribute()
    {
        if (!array_key_exists('quizzResults2WeeksCount', $this->relations))
            $this->load('quizzResultsCount');
        $related = $this->getRelation('quizzResults2WeeksCount')->first();

        return ($related) ? (int)$related->count : 0;
    }

    public function scopeVisible($query)
    {
        return $query->where('lang', App::getLocale())
            ->join('quizzs', 'quizzs.id', '=', 'quizz_langs.quizz_id')
            ->where('quizzs.visible', true);
    }

    public function scopeVisibleSlug($query, $slug)
    {
        return $query->where('lang', App::getLocale())
            ->join('quizzs', 'quizzs.id', '=', 'quizz_langs.quizz_id')
            ->where('quizzs.visible', true)
            ->where('quizzs.slug', $slug);
    }

    public function getTranslationsAttribute()
    {
        if (!array_key_exists('quizzTranslations', $this->relations))
            $this->load('quizzTranslations');
        $related = $this->getRelation('quizzTranslations')->lists('translation', 'slug');
        $translations = new Collection();
        foreach($related as $key => $value){
            $translations->$key = $value;
        }
        return ($translations) ? $translations : 'NaN';
    }
}
