<?php

namespace App\Quizz;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Quizz extends Model
{

    protected $table = 'quizzs';

    protected $fillable = [
        'id', 'slug', 'visible', 'image', 'created_at',
    ];

    public function scopeVisible($query){
        return $query->join('quizz_langs', 'quizzs.id', '=', 'quizz_langs.quizz_id')
            ->where('quizz_langs.lang', strtolower(App::getLocale()))
            ->where('quizz_langs.visible', true)
            ->where('quizzs.visible', true);
    }

    public function quizzLangs(){
        return $this->hasMany('App\Quizz\QuizzLang');
    }

    public function answers(){
        return $this->hasMany('App\Quizz\QuizzAnswer');
    }
}
