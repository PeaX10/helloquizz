<?php

namespace App\Quizz;

use Illuminate\Database\Eloquent\Model;

class QuizzAnswer extends Model
{
    protected $table = 'quizz_answers';

    protected $fillable = ['id', 'background', 'quizz_id', 'visible', 'builder'];

    public function quizz(){
        return $this->belongsTo('App\Quizz\Quizz');
    }

}
