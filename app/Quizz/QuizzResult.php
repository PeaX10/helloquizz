<?php

namespace App\Quizz;

use Illuminate\Database\Eloquent\Model;

class QuizzResult extends Model
{
    protected $table = 'quizz_results';

    protected $fillable = [
        'id', 'user_id', 'quizz_id', 'ip', 'slug', 'image', 'created_at'
    ];

    public function quizzLang(){
        return $this->belongsTo('App\Quizz\QuizzLang');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
