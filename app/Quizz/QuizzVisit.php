<?php

namespace App\Quizz;

use Illuminate\Database\Eloquent\Model;

class QuizzVisit extends Model
{
    protected $table = 'quizz_visits';

    protected $fillable = [
        'id', 'user', 'quizz', 'ip', 'created_at',
    ];

    public function quizzLang(){
        return $this->belongsTo('App\Quizz\QuizzLang');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
